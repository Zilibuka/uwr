var baseVal = '0.000.000.000',
        calculatingForm = $('.calculating-form'),
        formCurrency = calculatingForm.data('currency'),
        firstCoef = calculatingForm.data('coef1'),
        secondCoef = calculatingForm.data('coef2'),
        thirdCoef = calculatingForm.data('coef3'),
        scrollableTarget = $(".nice-select"),
        rowLabel = $('.row-label');

var FormulaHelper = {
    calculateResult: function (data) {
        var incomeApproach = Number(((Math.round(((data.firstInput - data.secondInput - data.thirdInput - data.fourthInput) + Number(data.fifthInput) + Number(data.sixthInput)) * firstCoef) * secondCoef) / thirdCoef).toFixed(1)) + Number(data.tenthInput); //if wrong number - need to check last operation with plus tenth input

        var assetValueMethod = (Number(data.seventhInput) + Number(data.eightInput)) - data.ninthInput;

        var multiplicationMethods = Math.round(data.selectDataValue * data.firstInput);

        var totalPrice = Number(((Number(incomeApproach) + Number(assetValueMethod) + Number(multiplicationMethods)) / 3).toFixed(1));

        return {
            first: incomeApproach,
            second: assetValueMethod,
            third: multiplicationMethods,
            fourth: totalPrice
        }
    },
    uwrNumberFormat: function (unformated, unformatedBaseValue) {
        var baseSize = 13,
                preformated = Number(unformated).toLocaleString('de-DE'),
                preformatedSize = preformated.length,
                formated = preformated,
                newFormatedText = baseVal.substring(0, baseSize - preformatedSize);

        if (unformatedBaseValue) {
            return newFormatedText;
        } else {
            return formated;
        }
    },
    ifIndividualSelected: function (individualSelected) {
        var $factorValue = $('.factor-value'),
                $factorValueInputBlock = $('.factor-value-input-block'),
                $factorValueSpan = $factorValue.find('span'),
                $selectFactor = $('.select-factor');

        $selectFactor.css('display', 'block');
        if ((individualSelected).hasClass('individual')) {
            $factorValue.hide();
            $factorValueInputBlock.css('display', 'block');
        } else {
            $factorValue.css('display', 'block');
            $factorValueInputBlock.hide();
            $factorValueSpan.text(individualSelected.val());
        }
        if ((individualSelected.val()).length == 0) {
            $factorValueSpan.text(0)
        }
    }
}

var UI = {
    scrollToError: function () {
        scrollableTarget = $(".nice-select");

        if (scrollableTarget.length) {
            $('html, body').animate({
                scrollTop: scrollableTarget.offset().top
            }, 2000);
        }
    },
    startLoader: function () {
        $('body').addClass('calculating');
    },
    finishLoader: function (animationTime) {
        setTimeout(function () {
            $('body').removeClass('calculating');
            $('.price-form-total, .clear-inputs').css('display', 'block');
            $('.form-actions').hide();
        }, animationTime);
    },
    showErrorMessage: function (errorStatus) {
        var $errorMessageBlock = $('.error-message');

        if (errorStatus) {
            $errorMessageBlock.css('display', 'block');
        } else {
            $errorMessageBlock.hide()
        }
    },
    showAdditionalBlock: function (checkStatus) {
        var $blockWithLargeForm = $('.large-form');

        if (checkStatus) {
            $blockWithLargeForm.slideToggle();
        } else {
            $blockWithLargeForm.slideToggle();
        }
    },
    clearInputFields: function (formTotalBlock) {
        $('.formular-form-row input').not('.factor-custom-value').val('');
        $('.form-actions, .select-factor').css('display', 'block');
        $('.input-bg-value .new-value, .input-bg-value .base-value').text(baseVal).css('color', '#C8CCD0');
        formTotalBlock.hide();
        $('.price-form-total').hide();
        $(('span.new-value')).css('font-family', 'PT-Mono-regular');
        $(".formular-custom-select option[value='']").prop('selected', true);
        $('.calculating-form').data("select-value", 0);
        $('.factor-value span').text(0);
        $('.nice-select .list li').removeClass('focus selected');
        $('.nice-select .list li:first-child').addClass('focus selected');
        $('.formular-custom-select .current').text($('.nice-select .list li:first-child').text());
    },
    showDesktopTooltips: function (tooltipBlock, e) {
        var tooltip = tooltipBlock.find('.tooltip-info'),
                additionaParentBlock = tooltipBlock.parent().parent();
        var tooltipX = e.pageX - tooltipBlock.offset().left;

        var tooltipY = e.pageY - tooltipBlock.offset().top;
        var tooltipOffsetY = (tooltipBlock.offset().top - tooltipY);

        tooltip.css({
            'opacity': '1',
            top: function () {
                return tooltipY;
            },
            left: function () {
                return tooltipX;
            }
        })

        if (additionaParentBlock.hasClass('right-additional-block')) {
            tooltip.css('left', (e.pageX * 0.1) - 50);
        }
    },
    showMobileTooltip: function (tooltipBlock) {
        var tooltip = tooltipBlock.find('.tooltip-info');

        $('.tooltip-info').hide();

        tooltip.css('display', 'block');
        tooltip.css('opacity', '1');
    },
    //new
    getCalculationData: function () {
        return {
            selectValue: $('.formular-custom-select').find(":selected").val(),
            selectDataValue: $('.calculating-form').data("select-value"),
            firstInput: $('.firstInput').val(),
            secondInput: $('.secondInput').val(),
            thirdInput: $('.thirdInput').val(),
            fourthInput: $('.fourthInput').val(),
            fifthInput: $('.fifthInput').val(),
            sixthInput: $('.sixthInput').val(),
            seventhInput: $('.seventhInput').val(),
            eightInput: $('.eightInput').val(),
            ninthInput: $('.ninthInput').val(),
            tenthInput: $('.tenthInput').val()
        }
    },
    drawResult: function (data) {

        $('.first-total').text((data.first).toLocaleString('de-DE'));

        $('.second-total').text((data.second).toLocaleString('de-DE'));

        $('.third-total').text((data.third).toLocaleString('de-DE'));

        $('.fourth-total').text((data.fourth).toLocaleString('de-DE'));
    }
}


$(document).ready(function () {
    // Init the nice select plugin
    $('.formular-custom-select').niceSelect();

    $('.form-currency').text(formCurrency);

    $('.formular-custom-select').on('change', function (e) {
        $('.calculating-form').data("select-value", Number(this.value))

        //checking fot the individual option
        var individualOption = this.options[e.target.selectedIndex];

        FormulaHelper.ifIndividualSelected($(individualOption))

    });

    //get factor custom select value
    $('.factor-custom-value').on('change', function () {

        $('.calculating-form').data("select-value", Number(this.value))
    })

    $(".calculating-form").validate({
        ignore: [],
        errorPlacement: function (error, element) {
            return false;
        },
        invalidHandler: function (form, validator) {
            UI.showErrorMessage(true);
            UI.scrollToError();
        },
        submitHandler: function (form) {

            UI.startLoader();

            UI.showErrorMessage(false);

            UI.drawResult(
                    FormulaHelper.calculateResult(UI.getCalculationData())
                    )

            UI.finishLoader(5000);

        }
    }
    );

    // Only number values for number fields
    $('.row-label-info input').on('keypress', function (event) {
        var keycode = event.keyCode;

        event = (event) ? event : event;

        if (!((keycode > 95 && keycode < 106)
                || (keycode > 47 && keycode < 58)
                || keycode == 8)) {
            return false;
        }
    })
    // if need max value in the input field
    $('.row-label-info input').not('.factor-custom-value').on('input', function (e) {
        var attr = $(this).attr('maxlength');
        if (typeof attr !== typeof undefined && attr !== false) {
            if (this.value > this.maxLength) {
                this.value = this.value.slice(0, 10)
            }
        }
    })
    // Showing additional form with additional inputs
    $('.form-row-radio input').on('change', function () {
        if ($(this).is(':checked')) {
            UI.showAdditionalBlock(true);
        } else {
            UI.showAdditionalBlock();
        }
    });

    //tooltips
    function initTooltipClose() {
        $(document).on('click', '[data-role="tooltip-close"]', function (event) {
            $(event.target).closest('[data-role="tooltip"]').hide();
        })
    }
    initTooltipClose();


    // Creating tooltips for the mobile and desktop
    if (window.matchMedia('(min-width: 993px)').matches) {
        rowLabel.hover(function () {
            var tooltip = $(this).find('.tooltip-info');
            tooltip.addClass('is-active')

            $(this).mousemove(function (e) {
                UI.showDesktopTooltips($(this), e)
            })
        });
        rowLabel.mouseleave(function () {
            var tooltip = $(this).find('.tooltip-info');
            tooltip.css('opacity', '0');
            tooltip.removeClass('is-active')
        });
    } else {
        rowLabel.on('click', function () {
            UI.showMobileTooltip($(this));
        });
    }

    //formating inputs
    $('.row-label-info input').keyup(function (e) {

        var $this = $(this),
                currentVal = $this.val(),
                bgNumbers = $this.parent().find('.input-bg-value'),
                bgNumbersBaseValue = bgNumbers.find('span.base-value'),
                bgNumbersNewValue = bgNumbers.find('span.new-value');

        //fix for mobile devices (need to fix it)
        if ((Number(currentVal).toLocaleString('de-DE').length) > 13) {
            return false;
        }

        if (Number(currentVal) > 0) {
            bgNumbersNewValue.css({
                'color': '#00AAAA',
                'font-family': 'PT-Mono-bold'
            });

            if (bgNumbers.hasClass('red-value')) {
                bgNumbersNewValue.css('color', '#FF0000');
            }
        } else {
            bgNumbersNewValue.css({
                'color': '#C8CCD0',
                'font-family': 'PT-Mono-regular'
            });
        }

        bgNumbersBaseValue.text(FormulaHelper.uwrNumberFormat(currentVal, true));
        bgNumbersNewValue.text(FormulaHelper.uwrNumberFormat(currentVal));
    });

    // Fake cursor for inputs
    $(".row-label-info").delegate("input", "focus blur", function () {
        var elem = $(this),
                elemParent = elem.parent(),
                currentInputBg = elemParent.find('.input-bg-value');
        setTimeout(function () {
            currentInputBg.toggleClass("focused", elem.is(":focus"));
        }, 0);
    });


    //clearing input numbers
    $('.clear-inputs').on('click', function () {
        UI.clearInputFields($(this));
        UI.scrollToError();
    });

});