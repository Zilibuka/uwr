<?php

/*
  Plugin Name:  UWR-Formular
  Description:  Entrepreneur well advised
  Version:      1.0.0
  Author:       QBeeS Solutions
  Author URI:   http://qbees.pro
  License:      GPL2
  License URI:  https://www.gnu.org/licenses/gpl-2.0.html
  Text Domain:  uwrf
  Domain Path:  /languages

  QB Adaptive Images is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  any later version.

  QB Adaptive Images is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with QB Adaptive Images. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */


if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
if (!function_exists('acf')) {
    echo 'UWR-Formular: Plugin ACF pro is required.';
    exit;
}

define('UWRF__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('UWRF__PLUGIN_URI', plugin_dir_url(__FILE__));

require_once UWRF__PLUGIN_DIR.'/admin/import_acf_data.php';

// Enable acf options page
if (function_exists('acf_add_options_page')) {
    $parent = acf_add_options_page(array(
        'page_title' => 'UWR Formular',
        'menu_title' => 'UWR Formular',
        'menu_slug' => 'uwr-theme-options',
        'capability' => 'edit_posts',
        'parent_slug' => '',
        'position' => false,
    ));
}

function uwrf_public_enqueues() {
    wp_enqueue_script('uwrf-jquery-validation', UWRF__PLUGIN_URI . '/public/js/jquery.validate.js', '', '', true);
    wp_enqueue_script('uwrf-jquery-nice-select', UWRF__PLUGIN_URI . '/public/js/jquery.nice-select.min.js', '', '', true);
    wp_enqueue_script('uwrf-main', UWRF__PLUGIN_URI . '/public/js/main.js', '', '', true);

    wp_enqueue_style('uwrf-styles', UWRF__PLUGIN_URI . '/public/css/fonts.css');
    wp_enqueue_style('uwrf-formular-styles', UWRF__PLUGIN_URI . '/public/css/formular-styles.css');
    wp_enqueue_style('uwrf-uncode-fix', UWRF__PLUGIN_URI . '/public/css/uncode-fix.css');
    wp_enqueue_style('uwrf-nice-select', UWRF__PLUGIN_URI . '/public/css/nice-select.css');
}

add_action('wp_enqueue_scripts', 'uwrf_public_enqueues');

function uwrf_form($atts) {
    $uwrf_form_currency = get_field('uwr_currency_symbol', 'option');
    $uwrf_form_title = '';
    $uwrf_form_coef1 = get_field('uwr_сoefficient_1', 'option');
    $uwrf_form_coef2 = get_field('uwr_сoefficient_2', 'option');
    $uwrf_form_coef3 = get_field('uwr_сoefficient_3', 'option');
    $uwrf_form_options = '';

    if (get_field('uwr_form_title', 'option')) {
        $uwrf_form_title = '<h1>' . get_field('uwr_form_title', 'option') . '</h1>';
    }

    if (have_rows('uwr_business_types', 'option')) {
        while (have_rows('uwr_business_types', 'option')) {
            the_row();
            $uwrf_form_options .= '<option value="' . get_sub_field('coefficient_value') . '">' . get_sub_field('business_type') . '</option>';
        }
        $uwrf_form_options .= '<option value="1" class="individual" >Individuell</option>';
    }

    return '<div class="price-form">
        <!--' . $uwrf_form_title . '-->
        <form action="" data-select-value="" data-currency="' . $uwrf_form_currency . '" data-coef1="' . $uwrf_form_coef1 . '" data-coef2="' . $uwrf_form_coef2 . '" data-coef3="' . $uwrf_form_coef3 . '" class="calculating-form">
            <div class="standart-form-inputs">
                <h2>'.get_field('uwr_form_title', 'option').'</h2>
                <div class="formular-form-row select-row">
                    <div class="row-label">
                        <label><span>*</span>'.get_field('uwr_f1_label', 'option').'<span class="mobile-label-notice">*</span></label>
                        <div class="tooltip-block">
                            <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img.svg" alt="">
                            <div class="tooltip-info" data-role="tooltip">
                                '.get_field('uwr_f1_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row-label-info row-label-select">
                        <select class="formular-custom-select"  required="required" name="factor" >
                            <option data-display="" value="">Bitte auswählen</option>
                            '.$uwrf_form_options.'
                        </select>
                        <div class="select-factor">
                            <p>Faktor</p>
                            <div class="factor-value">
                                <span>0</span>
                            </div>
                            <div class="factor-value factor-value-input-block">
                                <input type="number" value="1" class="factor-custom-value" min="0.1" inputmode="numeric" step="0.1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="short-form">
                    <div class="formular-form-row">
                        <div class="row-label">
                            <label><span>*</span>'.get_field('uwr_f2_label', 'option').' <i class="label-notice">'.get_field('uwr_f2_label_sub', 'option').'</i></label>
                            <div class="tooltip-block" data-role="tooltip">
                                <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img.svg" alt="">
                                <div class="tooltip-info" data-role="tooltip">
                                    '.get_field('uwr_f2_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row-label-info">
                            <div class="input-bg-value">
                                <div class="values-block">
                                    <!--<span class="new-value">0.000.000.000</span>-->
                                    <span class="new-value"></span>
                                    <span class="base-value">0.000.000.000</span>
                                </div>
                            </div>
                        <input type="number" required="required" class="firstInput" min="1" inputmode="numeric" pattern="[0-9]*" name="firstValue" maxlength="9999999999">
                            <!---<span class="form-currency"></span>-->
                        </div>
                    </div>
                    <div class="formular-form-row">
                        <div class="row-label">
                            <label><span>*</span>'.get_field('uwr_f3_label', 'option').'<i class="label-notice">'.get_field('uwr_f3_label_sub', 'option').'</i></label>
                            <div class="tooltip-block">
                                <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img.svg" alt="">
                                <div class="tooltip-info" data-role="tooltip">
                                    '.get_field('uwr_f3_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                </div>
                        </div>
                        </div>
                        <div class="row-label-info">
                            <div class="input-bg-value">
                                <div class="values-block">
                                    <span class="new-value"></span>
                                    <span class="base-value">0.000.000.000</span>
                                </div>
                            </div>
                            <input type="number" required="required" class="seventhInput" min="0" inputmode="numeric" pattern="[0-9]*" name="secondValue" maxlength="9999999999">
                        </div>
                    </div>
                    <div class="formular-form-row">
                        <div class="row-label">
                            <label><span>*</span>'.get_field('uwr_f4_label', 'option').'</label>
                            <div class="tooltip-block">
                                <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img.svg" alt="">
                                <div class="tooltip-info" data-role="tooltip">
                                    '.get_field('uwr_f4_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row-label-info">
                            <div class="input-bg-value">
                                <div class="values-block">
                                    <span class="new-value"></span>
                                    <span class="base-value">0.000.000.000</span>
                                </div>
                            </div>
                            <input type="number" required="required" class="eightInput" min="0" inputmode="numeric" pattern="[0-9]*" name="thirdValue" maxlength="9999999999">
                        </div>
                    </div>
                    <div class="formular-form-row">
                        <div class="row-label">
                            <label><span>*</span>'.get_field('uwr_f5_label', 'option').'</label>
                            <div class="tooltip-block">
                                <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img.svg" alt="">
                                <div class="tooltip-info" data-role="tooltip">
                                    '.get_field('uwr_f5_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row-label-info">
                            <div class="input-bg-value">
                                <div class="values-block">
                                    <span class="new-value"></span>
                                    <span class="base-value">0.000.000.000</span>
                                </div>
                            </div>
                            <input type="number" required class="tenthInput" min="0" inputmode="numeric" pattern="[0-9]*" name="fourthValue" maxlength="9999999999">
                        </div>
                    </div>
                    <div class="formular-form-row">
                        <div class="row-label">
                            <label><span>*</span>'.get_field('uwr_f6_label', 'option').'</label>
                            <div class="tooltip-block">
                                <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img.svg" alt="">
                                <div class="tooltip-info" data-role="tooltip">
                                    '.get_field('uwr_f6_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row-label-info">
                            <div class="input-bg-value red-value">
                            
                                <div class="values-block">
                                    <span class="new-value"></span>
                                    <span class="base-value">0.000.000.000</span>
                                </div>
                            </div>
                            <input type="number" required="required" class="ninthInput" min="0" inputmode="numeric" pattern="[0-9]*" name="fifthValue" maxlength="9999999999">
                        </div>
                    </div>
                    <div class="form-notice-block">
                        <p><span>*</span>'.get_field('uwr_annotation', 'option').'</p>
                    </div>
                </div>
            </div>
            <div class="additional-inputs-block">
                <div class="formular-form-row form-row-radio">
                    <h3>'.get_field('uwr_switcher', 'option').'</h3>
                    <div>
                        <!--<input type="radio" name="form-radio-check" id="without-long-form" value="0" checked>
                        <label for="without-long-form">nein</label>-->
                        <input type="checkbox" id="open-large-form">
                        <label for="open-large-form"></label>
                    </div>
                </div>
                    <div class="large-form">
                    <div class="left-additional-block">
                        <h3 class="red-text">wertmindernd</h3>
                        <div class="formular-form-row">
                            <div class="row-label">
                                <label>'.get_field('uwr_f7_label', 'option').'</label>
                                <div class="tooltip-block">
                                    <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img-blue.svg" alt="">
                                    <div class="tooltip-info" data-role="tooltip">
                                        '.get_field('uwr_f7_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-label-info">
                                <div class="input-bg-value red-value">
                                    <div class="values-block">
                                        <span class="new-value"></span>
                                        <span class="base-value">0.000.000.000</span>
                                    </div>
                                </div>
                                <input type="number" class="secondInput" min="0" inputmode="numeric" pattern="[0-9]*" maxlength="9999999999">
                            </div>
                        </div>
                        <div class="formular-form-row">
                            <div class="row-label">
                                <label>'.get_field('uwr_f8_label', 'option').'</label>
                                <div class="tooltip-block">
                                    <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img-blue.svg" alt="">
                                    <div class="tooltip-info" data-role="tooltip">
                                        '.get_field('uwr_f8_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-label-info">
                                <div class="input-bg-value red-value">
                                    <div class="values-block">
                                        <span class="new-value"></span>
                                        <span class="base-value">0.000.000.000</span>
                                    </div>
                                </div>
                                <input type="number" class="fourthInput" min="0" inputmode="numeric" pattern="[0-9]*" maxlength="9999999999">
                            </div>
                        </div>
                        <div class="formular-form-row">
                            <div class="row-label">
                                <label>'.get_field('uwr_f9_label', 'option').'</label>
                                <div class="tooltip-block">
                                    <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img-blue.svg" alt="">
                                    <div class="tooltip-info" data-role="tooltip">
                                        '.get_field('uwr_f9_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-label-info">
                                <div class="input-bg-value red-value">
                                    <div class="values-block">
                                        <span class="new-value"></span>
                                        <span class="base-value">0.000.000.000</span>
                                    </div>
                                </div>
                                <input type="number" class="thirdInput" min="0" inputmode="numeric" pattern="[0-9]*" maxlength="9999999999">
                            </div>
                        </div>
                    </div>
                    <div class="right-additional-block">
                        <h3>werterhöhend</h3>
                        <div class="formular-form-row">
                            <div class="row-label">
                                <label>'.get_field('uwr_f10_label', 'option').'</label>
                                <div class="tooltip-block">
                                    <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img-blue.svg" alt="">
                                    <div class="tooltip-info" data-role="tooltip">
                                        '.get_field('uwr_f10_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-label-info">
                                <div class="input-bg-value">
                                    <div class="values-block">
                                        <span class="new-value"></span>
                                        <span class="base-value">0.000.000.000</span>
                                    </div>
                                </div>
                                <input type="number" class="fifthInput" min="0" inputmode="numeric" pattern="[0-9]*" maxlength="9999999999">
                            </div>
                        </div>
                        <div class="formular-form-row">
                            <div class="row-label">
                                <label>'.get_field('uwr_f11_label', 'option').'</label>
                                <div class="tooltip-block">
                                    <img src="'.UWRF__PLUGIN_URI.'/public/img/tooltip-img-blue.svg" alt="">
                                    <div class="tooltip-info" data-role="tooltip">
                                        '.get_field('uwr_f11_tooltip', 'option').'
                                <div class="close-tooltip" data-role="tooltip-close"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row-label-info">
                                <div class="input-bg-value">
                                    <div class="values-block">
                                        <span class="new-value"></span>
                                        <span class="base-value">0.000.000.000</span>
                                    </div>
                                </div>
                                <input type="number" class="sixthInput" min="0" inputmode="numeric" pattern="[0-9]*" maxlength="9999999999">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="price-form-total">
                <h4>'.get_field('uwr_results_title', 'option').'</h4>
                <div class="form-total-block">
                    <p class="total-block-name">'.get_field('uwr_result1_label', 'option').'</p>
                    <span class="total-block-price first-total">0</span>
                </div>
                <div class="form-total-block">
                    <p class="total-block-name">'.get_field('uwr_result2_label', 'option').'</p>
                    <span class="total-block-price second-total">0</span>
                </div>
                <div class="form-total-block">
                    <p class="total-block-name">'.get_field('uwr_result3_label', 'option').'</p>
                    <span class="total-block-price third-total">0</span>
                </div>
                <div class="form-total-block sum-total-form-block">
                    <p class="total-block-name">'.get_field('uwr_result4_label', 'option').'</p>
                    <span class="total-block-price fourth-total">0</span>
                    <span class="form-currency"></span>
                </div>
            </div>
            <div class="form-actions">
                <div class="submit-form-block">
                    <input type="submit" value="'.get_field('uwr_button', 'option').'" class="calculate-form-btn">
                </div>
                <div class="error-message">
                    '.get_field('uwr_error', 'option').'
                </div>
            </div>
        </form>
    </div>
        <button class="clear-inputs">'.get_field('uwr_clear_fields_button_text', 'option').'</button>';
}

add_shortcode('uwrf_form', 'uwrf_form');
